package com.emp.friskyplayer.utils;

public class ServiceActionConstants {

	// Intent actions that service can handle
	public static final String ACTION_TOGGLE_PLAYBACK = "com.emp.friskyplayer.services.action.TOGGLE_PLAYBACK";
	public static final String ACTION_PLAY = "com.emp.friskyplayer.services.action.PLAY";
	public static final String ACTION_STOP = "com.emp.friskyplayer.services.action.STOP";
	public static final String ACTION_CHANGE_QUALITY = "com.emp.friskyplayer.services.action.QUALITY";
	public static final String STREAM_TITLE = "com.emp.friskyplayer.services.communication.STREAM_TITLE";
	public static final String BUFFER_PROGRESS = "com.emp.friskyplayer.services.communication.BUFFER_PROGRESS";
	public static final String LOADING = "com.emp.friskyplayer.services.communication.LOADING";
}
