package com.emp.friskyplayer.utils;

public class PreferencesConstants {

	// PREFERENCE KEYS
	public static final String CONNECTION_TYPE= "connection";
	public static final String STREAMING_URL= "streaming_url";
	public static final String QUALITY= "quality";
	public static final String SLEEP_MODE= "sleep_mode";
	public static final String SLEEP_MODE_TIMEOUT= "sleep_mode_timeout";
	public static final String PLAYER_STATE= "player_state";
	public static final String STREAMING_TITLE= "streaming_title";
	
	// PREFERENCE DEFAULT VALUES
	public static final String STREAMING_URL_HQ= "http://205.188.215.229:8024";
	public static final String STREAMING_URL_LQ= "http://stream-07.shoutcast.com:8004/frisky_radio_aac+_128kbps";

	public static final String QUALITY_HQ= "quality_hq";
	public static final String QUALITY_LQ= "quality_lq";
	public static final String QUALITY_AUTO= "quality_auto";

}
