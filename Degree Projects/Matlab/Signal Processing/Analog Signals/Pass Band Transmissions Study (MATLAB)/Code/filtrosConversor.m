%% Carga los filtros que se van a usar para pasar a cuadratura para cada
%% una de las señales

load('filtros/conversion/armonico.mat');
load('filtros/conversion/suma.mat');
load('filtros/conversion/diente.mat');
load('filtros/conversion/triangular.mat');
load('filtros/conversion/cuadrada.mat');
load('filtros/conversion/am.mat');
load('filtros/conversion/fm.mat');
