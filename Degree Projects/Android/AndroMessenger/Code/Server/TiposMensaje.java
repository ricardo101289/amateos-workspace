
public class TiposMensaje {

	//Tipos de mensaje que envía el servidor.
	public static String mBienvenidaServidor = "La conexión con el servidor se ha realizado correctamente";
	public static String mDespedidaServidor = "200";
	
	public static String mRegistroCorrecto = "200";
	public static String mRegistroIncorrecto = "400.";
	
	public static String mRespuestaOK = "200";
	public static String mRespuestaFallo = "400";
	public static String mRespuestaLocalizacion = "LOCALIZACION"; 
	public static String mRespuestaListaContactos = "LISTADO"; 
	public static String mRespuestaListaContactosConectados = "CONECTADOS"; 
	public static String mRespuestaApodo = "APODO"; 
	public static String mRespuestaCambioApodo = "NUEVOAPODO"; 


	
	//Tipos de mensaje que envía el cliente.
	public static String mBienvenidaCliente = "Cliente desea conectarse.";
	public static String mRegistrar = "REGISTRAR";
	public static String mSolicitarLocalizacion = "LOCALIZAR";
	public static String mAnadirContacto = "ANADIRCONTACTO";
	public static String mEliminarContacto = "ELIMINAR";
	public static String mSolicitudListaContactos = "LISTACONTACTOS";
	public static String mSolicitudApodo = "APODO";
	public static String mSolicitudListaContactosConectados = "LISTACONTACTOSCONECTADOS";
	public static String mCierreSesion = "CERRAR";
	public static String mSolicitudCambioApodo = "CAMBIOAPODO"; 

	
}
