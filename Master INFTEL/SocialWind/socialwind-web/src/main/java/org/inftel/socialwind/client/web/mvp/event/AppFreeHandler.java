package org.inftel.socialwind.client.web.mvp.event;

import com.google.gwt.event.shared.EventHandler;

public interface AppFreeHandler extends EventHandler{
	  public void onAppFreeEvent(AppFreeEvent event);
}
