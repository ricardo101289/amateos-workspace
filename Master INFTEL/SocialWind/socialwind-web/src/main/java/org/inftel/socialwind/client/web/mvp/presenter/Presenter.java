package org.inftel.socialwind.client.web.mvp.presenter;

public interface Presenter {
    public void bind();
}
