package org.inftel.socialwind.client.web.mvp.presenter;

public interface SesionListPresenter extends Presenter {
    void onCargarSesiones();
}
