/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inftel.scrum.servlet;

import java.util.*;
import org.inftel.scrum.modelXML.*;

/**
 *
 * @author Fernando
 */
public class Constantes {

    public static final int VER_PROYECTO = 1;
    public static final int OBTENER_USUARIO = 2;
    public static final int CONTAR_USUARIOS = 3;
    public static final int CREAR_USUARIO = 4;
    public static final int EDITAR_USUARIO = 5;
    public static final int LISTAR_USUARIOS = 6;
    public static final int BUSCAR_USUARIO_POR_NOMBRE = 7;
    public static final int BUSCAR_USUARIO_POR_EMAIL = 8;
    public static final int BUSCAR_USUAIRO_POR_PROYECTO = 9;
    public static final int MAX_ID_USUARIO = 10;
    public static final int ELIMINAR_USUARIO = 11;
    public static final int CONTAR_PROYECTO = 12;
    public static final int BUSCAR_PROYECTO_POR_NOMBRE = 13;
    public static final int BUSCAR_PROYECTO_REPETIDO = 14;
    public static final int CREAR_PROYECTO = 15;
    public static final int EDITAR_PROYECTO = 16;
    public static final int MAX_ID_PROYECTO = 17;
    public static final int ELIMINAR_PROYECTO = 18;
    public static final int CONTAR_TAREAS = 19;
    public static final int CREAR_TAREA = 20;
    public static final int BUSCAR_TAREA_POR_NOMBRE = 21;
    public static final int BUSCAR_TAREA_REPETIDA = 22;
    public static final int BUSCAR_TAREA_POR_USUARIO = 23;
    public static final int BUSCAR_TAREAS_TODO = 24;
    public static final int BUSCAR_TAREAS_DOING = 25;
    public static final int BUSCAR_TAREAS_DONE = 26;
    public static final int MAX_ID_TAREA = 27;
    public static final int ELIMINAR_TAREA = 28;
    public static final int CONTAR_SPRINTS = 29;
    public static final int CREAR_SPRINT = 30;
    public static final int EDITAR_SPRINT = 31;
    public static final int BUSCAR_SPRINTS = 32;
    public static final int LISTAR_SPRINTS = 33;
    public static final int MAX_ID_SPRINT = 34;
    public static final int ELIMINAR_SPRINT = 35;
    public static final int DO_LOGIN = 36;
    public static final int GET_BURNDOWN = 38;
    public static final int VER_BACKLOG = 39;
    public static final int GET_PIE = 40;
    public static final int GET_BURNUP = 41;
    public static final int CREAR_REUNIONES = 42;
    public static final int BUSCAR_REUNIONES = 43;
    public static final int GET_PDF_USUARIO = 44;
    public static final int GET_PDF_PROYECTO = 45;
    public static final int GET_PDF_BACKLOG = 46;
    public static final int GET_PDF_SPRINTS = 47;
    public static final int GET_PDF_LOGS = 48;
    public static final int GET_MENSAJES_ENVIADOS = 49;
    public static final int GET_MENSAJES_RECIBIDOS = 50;
    public static final int BUSCAR_USUARIO_GRUPO = 51;
    public static final int DO_LOGOUT = 52;
    public static final int GET_FORECAST = 53;
    public static final int GET_SPEED = 54;
    public static final int GET_DELAY = 55;
    public static final int BUSCAR_TAREAS_SPRINTS = 56;
    public static final int EDITAR_TAREA = 57;
    public static final int OBTENER_FECHA_INICIO_TAREA = 58;
    public static final int OBTENER_FECHA_FIN_TAREA = 59;
    public static final int OBTENER_USUARIO_TAREA = 60;
    public static final int EDITAR_LISTA_TAREAS=61;
    public static final int CREAR_MENSAJE=62;
    public static final int SET_MENSAJES_NOTIFICA = 63;
    public static final int SET_MENSAJES_LEIDO = 64;
    public static final int ELIMINAR_MENSAJE = 65;
}
