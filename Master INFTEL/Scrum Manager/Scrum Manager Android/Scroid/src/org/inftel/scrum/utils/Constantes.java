package org.inftel.scrum.utils;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Fernando
 */
public class Constantes {

	public static final String VER_PROYECTO = "1";
	public static final String OBTENER_USUARIO = "2";
	public static final String CONTAR_USUARIOS = "3";
	public static final String CREAR_USUARIO = "4";
	public static final String EDITAR_USUARIO = "5";
	public static final String LISTAR_USUARIOS = "6";
	public static final String BUSCAR_USUARIO_POR_NOMBRE = "7";
	public static final String BUSCAR_USUARIO_POR_EMAIL = "8";
	public static final String BUSCAR_USUARIO_POR_PROYECTO = "9";
	public static final String MAX_ID_USUARIO = "10";
	public static final String ELIMINAR_USUARIO = "11";
	public static final String CONTAR_PROYECTO = "12";
	public static final String BUSCAR_PROYECTO_POR_NOMBRE = "13";
	public static final String BUSCAR_PROYECTO_REPETIDO = "14";
	public static final String CREAR_PROYECTO = "15";
	public static final String EDITAR_PROYECTO = "16";
	public static final String MAX_ID_PROYECTO = "17";
	public static final String ELIMINAR_PROYECTO = "18";
	public static final String CONTAR_TAREAS = "19";
	public static final String CREAR_TAREA = "20";
	public static final String BUSCAR_TAREA_POR_NOMBRE = "21";
	public static final String BUSCAR_TAREA_REPETIDA = "22";
	public static final String BUSCAR_TAREA_POR_USUARIO = "23";
	public static final String BUSCAR_TAREAS_TODO = "24";
	public static final String BUSCAR_TAREAS_DOING = "25";
	public static final String BUSCAR_TAREAS_DONE = "26";
	public static final String MAX_ID_TAREA = "27";
	public static final String ELIMINAR_TAREA = "28";
	public static final String CONTAR_SPRINTS = "29";
	public static final String CREAR_SPRINTS = "30";
	public static final String EDITAR_SPRINTS = "31";
	public static final String BUSCAR_SPRINTS = "32";
	public static final String LISTAR_SPRINTS = "33";
	public static final String MAX_ID_SPRINTS = "34";
	public static final String ELIMINAR_SPRINTS = "35";
	public static final String URL_SERVLET = "http://192.168.1.143:8080/ProyectoScrum-war/ServletUsuario";
	public static final String DO_LOGIN = "36";
	public static final int CONNECTION_DIALOG = 37;
	public static final String ACCOUNT_TYPE = "org.inftel.scrum"; // cambiar por
																	// raiz de
																	// nuestro
																	// proyecto
	public static final String AUTHTOKEN_TYPE = "org.inftel.scrum";// cambiar
																	// por raiz
																	// de
																	// nuestro
																	// proyecto
	public static final String PARAM_CONFIRMCREDENTIALS = "confirmCredentials";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_USERNAME = "username";
	public static final String PARAM_AUTHTOKEN_TYPE = "authtokenType";
	public static final String USER_AGENT = "AuthenticationService/1.0";
	public static final int REGISTRATION_TIMEOUT = 30 * 1000; // ms
	public static final String GET_BURNDOWN = "38";
	public static final String VER_BACKLOG = "39";
	public static final String GET_PIE = "40";
	public static final String GET_BURNUP = "41";
	public static final String CREAR_REUNIONES = "42";
	public static final String BUSCAR_REUNIONES = "43";
	public static final String GET_PDF_USUARIO = "44";
	public static final String GET_PDF_PROYECTO = "45";
	public static final String GET_PDF_BACKLOG = "46";
	public static final String GET_PDF_SPRINTS = "47";
	public static final String GET_PDF_LOGS = "48";
	public static final String GET_MENSAJES_ENVIADOS = "49";
	public static final String GET_MENSAJES_RECIBIDOS = "50";
	public static final String BUSCAR_USUARIO_GRUPO = "51";
	public static final String DO_LOGOUT = "52";
	public static final String GET_FORECAST = "53";
	public static final String GET_SPEED = "54";
	public static final String GET_DELAY = "55";
	public static final String BUSCAR_TAREAS_SPRINTS = "56";
	public static final String EDITAR_TAREA = "57";
	public static final String OBTENER_FECHA_INICIO_TAREA = "58";
	public static final String OBTENER_FECHA_FIN_TAREA = "59";
	public static final String OBTENER_USUARIO_TAREA = "60";
	public static final String EDITAR_LISTA_TAREAS="61";
	public static final String CREAR_MENSAJE = "62";
	public static final String SET_MENSAJES_NOTIFICA = "63";
	public static final String SET_MENSAJES_LEIDO = "64";
	public static final String ELIMINAR_MENSAJE = "65";
	

}
